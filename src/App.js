import React, { Component } from 'react';

// Services
import { speak, getVoceSettings } from './services/voice';

// Components
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextArea from './components/TextArea/TextArea';
import ErrorSound from './assets/sounds/wrongKeyError.wav';

// Constants
import { text } from './lessons-texts';

const appointmentObject = {
	appointment: {
		triggerInfo: {
			name: 'triggerInfo',
			success: true,
			triggers: [ 'first trigger', 'second trigger' ]
		},
		enter: {
			name: 'enter',
			id: 12,
			state: 'state_first'
		},
		viewModel: {
			name: 'viewModel',
			id: 12,
			state: 'state_first',
			testInsert: {
				name: 1
			}
		}
	}
};

function update(obj, data) {
	const initObj = JSON.parse(JSON.stringify(obj));

	function isObj(value) {
		return typeof value === 'object' && !value.length;
	}

	function updateObject(objectClone, updateData) {
		for (let updateKey in updateData) {
			if (objectClone[updateKey]) {
				if (isObj(updateData[updateKey])) {
					updateObject(objectClone[updateKey], updateData[updateKey]);
				} else {
					objectClone[updateKey] = updateData[updateKey];
				}
			} else {
				objectClone[updateKey] = updateData[updateKey];
			}
		}
	}

	updateObject(initObj, data);

	return initObj;
}

console.log(
	update(appointmentObject, {
		appointment: {
			triggerInfo: {
				success: false
			},
			viewModel: {
				name: 'name after updated',
				state: 'second_state_after_update',
				testInsert: {
					name: 'change name'
				}
			}
		},
		isSokets: true
	})
);

console.log('init obj ', appointmentObject);

class App extends Component {
	constructor() {
		super();
		this.state = {
			anchorEl: null,
			eventKey: '',
			index: 0,
			errorCounter: 0,
			currentVoice: null,
			voicesList: []
		};
		this.audio = new Audio(ErrorSound);
		getVoceSettings().then((voices) => {
			this.setState({ currentVoice: voices[1] });
		});
	}

	handleChangeVoice = (voice) => {
		console.log(voice);
	};

	handleClick = (event) => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleClose = () => {
		this.setState({ anchorEl: null });
	};

	onKeyPress = (key) => {
		const { index, errorCounter } = this.state;
		if (key === text[index]) {
			this.setState({ index: index + 1 }, () => {
				if (text[this.state.index]) {
					speak(text[this.state.index]);
				} else {
					alert('lesson was finished');
				}
			});
		} else {
			this.setState({ errorCounter: errorCounter + 1 }, () => {
				this.audio.volume = 0.1;
				this.audio.play();
			});
		}
	};

	render() {
		const { anchorEl, index, errorCounter, currentVoice, voicesList } = this.state;

		return (
			<div>
				<Button
					aria-owns={anchorEl ? 'simple-menu' : undefined}
					aria-haspopup="true"
					onClick={() => {
						speak('тестовое сообщение');
					}}
				>
					Open Menu
				</Button>
				{/* <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>Profile</MenuItem>
          <MenuItem onClick={this.handleClose}>My account</MenuItem>
          <MenuItem onClick={this.handleClose}>Logout</MenuItem>
        </Menu> */}

				{/* <Select
          value={currentVoice && currentVoice.voiceURI}
          onChange={this.handleChangeVoice}
        > */}
				{/* {voicesList.length
            ? voicesList.map(v => {
                <MenuItem value={v.voiceURI}>{v.voiceURI}</MenuItem>;
              })
            : null} */}
				{/* </Select> */}

				<p>Errors: {errorCounter}</p>

				<TextArea text={text} index={index} onKeyPress={this.onKeyPress} />
			</div>
		);
	}
}

export default App;
