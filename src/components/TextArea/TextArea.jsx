import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './TextArea.scss';

const setCharClass = (index, i) => {
	if (index === i) return 'active';
	if (index > i) return 'past';
	return '';
};

class TypeArea extends Component {
	state = { isFocus: false };

	static propTypes = {
		index: PropTypes.number,
		text: PropTypes.string.isRequired,
		onKeyPress: PropTypes.func
	};

	static defaultProps = {
		index: 0,
		text: ''
	};

	onFocus = () => {
		this.setState({ isFocus: true });
	};

	onBlur = () => {
		this.setState({ isFocus: false });
	};

	onKeyPress = (e) => {
		const { onKeyPress } = this.props;
		if (onKeyPress) {
			onKeyPress(e.key);
		}
	};

	renderText = () => {
		const { text, index } = this.props;
		const charArray = text.split('');
		return charArray.map((char, i) => (
			<span key={`char-${i}-${char}`} className={setCharClass(index, i)}>
				{char}
			</span>
		));
	};

	render() {
		const { isFocus } = this.state;
		return (
			<div className={`text-area_wrapper ${isFocus && 'active'}`}>
				<textarea onFocus={this.onFocus} onBlur={this.onBlur} onKeyPress={this.onKeyPress} />
				<div className="text">{this.renderText()}</div>
			</div>
		);
	}
}

export default TypeArea;
