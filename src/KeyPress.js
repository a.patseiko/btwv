import React from 'react';
import { keyHandler, KEYPRESS } from 'react-key-handler';
 
function Demo({ keyValue }) {
  return (
    <div>
      {keyValue === 's' && (
        <ol>
          <li>hello</li>
          <li>world</li>
        </ol>
      )}
    </div>
  );
}
 
export default keyHandler({ keyEventName: KEYPRESS, keyValue: 's' })(Demo);