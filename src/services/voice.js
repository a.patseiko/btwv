export const getVoceSettings = (voiceName = "") => {
  return new Promise(resolve => {
    window.speechSynthesis.onvoiceschanged = e => {
      // optionally filter returned voice by `voiceName`
      // resolve(
      //  window.speechSynthesis.getVoices()
      //  .filter(({name}) => /^en.+whisper/.test(name))
      // );
      resolve(window.speechSynthesis.getVoices());
    };
    window.speechSynthesis.getVoices();
  });
};

export const speak = text => {
  const message = text === " " ? "space" : text;
  const msg = new SpeechSynthesisUtterance();
  const voices = speechSynthesis.getVoices();
  //   if (voices instanceof Array && voices.length) {
  //     console.log(voices.map(v => v.voiceURI));
  //   }
  msg.voice = voices[15];
  // msg.voiceURI = "native";
  // msg.volume = 1;
  // msg.rate = 1;
  // msg.pitch = 1;
  msg.text = message;
  // msg.lang = "ru-RU"; // Язык
  speechSynthesis.speak(msg);
};
